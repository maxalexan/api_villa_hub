require('dotenv').config()
const express = require('express');
const cookieParser = require('cookie-parser');
const bodyParser = require("body-parser");
const logger = require('morgan');

const app = express();

app.use(logger('dev'));
app.use(bodyParser.json());

app.use(express.urlencoded({
    extended: true
}));
app.use(cookieParser());

app.get("/", (req, res) => {
    res.json({
        message: "API Villa Hub"
    });
});

const title = process.env.TITLE;
const port = process.env.PORT;
const baseUrl = process.env.URL + port;

app.use((req, res, next) => {
    res.header("Access-Control-Allow-Origin", "*");
    res.header(
        "Access-Control-Allow-Headers",
        "Origin, X-Requested-With, Content-Type, Accept, Authorization, x-access-token"
    );
    if (req.method === 'OPTIONS') {
        res.header('Access-Control-Allow-Methods', 'PUT, POST, PATCH, DELETE, GET');
        return res.status(200).json({});
    }
    next();
});
require('./app/router/router.js')(app);

app.listen(port, () => console.log(title + " run on " + baseUrl))