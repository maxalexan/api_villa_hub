<h1 align="center"> API Villa Hub </h1> 
Villa Hub merupakan aplikasi manajemen akomodasi berbasis cloud

## Build with
[![Express.js](https://img.shields.io/badge/express-4.17.1-yellow?style=rounded-square)](https://expressjs.com/en/starter/installing.html) [![Node.js](https://img.shields.io/badge/npm-6.9.0-greenstyle?rounded-square)](https://nodejs.org/) [![MySQL](https://img.shields.io/badge/mysql-2.17.1-blue?rounded-square)](https://www.npmjs.com/search?q=mysql) [![MySQL](https://img.shields.io/badge/body--parser-1.19.0-red?rounded-square)](https://www.npmjs.com/package/body-parser) [![Morgan](https://img.shields.io/badge/morgan-1.9.1-brightgreen?style=rounded-square)](https://www.npmjs.com/package/morgan) [![CORS](https://img.shields.io/badge/cors-2.8.5-lightgrey?style=rounded-square)](https://www.npmjs.com/package/cors) [![CORS](https://img.shields.io/badge/jsonwebtoken-8.5.1-yellowgreen?style=rounded-square)](https://www.npmjs.com/package/jsonwebtoken)

## Requirements
1. [Node JS](https://nodejs.org/en/download/)
2. [Sequelize](https://sequelize.org/)
3. [Express JS](https://expressjs.com/en/starter/installing.html)
4. [Postman](https://www.getpostman.com/)
5. Web Server (ex. xampp or mamp)

Before starting to clone repository, it's better to read and know **Node JS**, **REST API** and **Read the Documentation** about the requirements above

## Getting Started
- Clone this repository
- Open terminal and run 
-   `npm install` 
- create database in phpmyadmin 
- edit file configDatabase.json
- run sequelize db:migrate
- Turn on Web Server and MySQL, (Also can be done with third-party tools like XAMPP, MAMP, etc)
- Setup the database.
- Open **Postman** desktop application
- Choose HTTP Method and enter the request URL.(i.e. localhost:3000/auth/signup)
- Check all **Endpoints**

## Postman Collections
import file postman collection.json to postman

## Setup .env file
Copy and rename example-env.txt to .env

## Create secret key
Run node generate.js on terminal then copy the key into SECRET in file env

## Example generate model use sequelize
sequelize model:create --name account --attributes full_name:string,email:string,password:string,address:string,phone:integer,image:string,type_account:integer,is_active:integer,is_verified:integer 
