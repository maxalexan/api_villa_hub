const room = require('../models/room');

const Room = require('../models').room;

module.exports = {

  getById(req, res) {
    return Room
      .findByPk(req.params.id, {
        include: [],
      })
      .then((room) => {
        if (!room) {
          return res.status(404).send({
            response: 'Not Found',
            errors: 'Room Not Found',
          });
        }
        const status = {
          response: 'OK',
          status: room,
          errors: null
        }
        return res.status(200).send(status);
      })
      .catch((error) => {
        res.status(400).send({
          response: 'Bad Request',
          errors: error
        });
      });
  },

  list(req, res) {
    return Room
      .findAll({
        limit: 10,
        include: [],
        order: [
          ['createdAt', 'DESC']
        ],
      })
      .then(docs => {
        const statuses = {
          response: 'OK',
          count: docs.length,
          statuses: docs.map(doc => {
            return doc
          }),
          errors: null
        }
        res.status(200).send(statuses);
      })
      .catch((error) => {
        res.status(400).send({
          response: 'Bad Request',
          errors: error
        });
      });
  },

  
  add(req, res) {
    return Room
      .create({
        id_hotel: req.body.id_hotel,
        room_hotel: req.body.room_hotel,
        room_title: req.body.room_title,
        room_basic_price: req.body.room_basic_price,
        room_desc: req.body.room_desc,
        room_cap: req.body.room_cap,
        room_amenities: req.body.room_amenities,
        room_add: req.body.room_add,
        room_quantity: req.body.room_quantity,
        room_status: req.body.room_status,
        cover_img: req.body.cover_img
      })
      .then((doc) => {
        const status = {
          response: 'Room has Created',
          status: doc,
          errors: null
        }
        return res.status(201).send(status);
      })
      .catch((error) => {
        res.status(400).send({
          response: 'Bad Request',
          errors: error
        });
      });
  },

  update(req, res) {
    return Room
      .findByPk(req.params.id, {})
      .then(status => {
        if (!status) {
          return res.status(404).send({
            response: 'Bad Request',
            errors: 'Accomoddations Not Found',
          });
        }

        return status
          .update({
            id_hotel: req.body.id_hotel || room.id_hotel,
            room_hotel: req.body.room_hotel || room.room_hotel,
            room_title: req.body.room_title || room.room_title,
            room_basic_price: req.body.room_basic_price || room.room_basic_price,
            room_desc: req.body.room_desc || room.room_desc,
            room_cap: req.body.room_cap || room.room_cap,
            room_amenities: req.body.room_amenities || room.room_amenities,
            room_add: req.body.room_add || room.room_add,
            room_quantity: req.body.room_quantity || room.room_quantity,
            room_status: req.body.room_status || room.room_status,
            cover_img: req.body.cover_img || room.cover_img
          })
          .then((doc) => {
            const status = {
              response: 'OK',
              status: doc,
              errors: null
            }
            return res.status(200).send(status);
          })
          .catch((error) => {
            res.status(400).send({
              response: 'Bad Request',
              errors: error
            });
          });
      })
      .catch((error) => {
        res.status(400).send({
          response: 'Bad Request',
          errors: error
        });
      });
  },

  delete(req, res) {
    return Room
      .findByPk(req.params.id)
      .then(status => {
        if (!status) {
          return res.status(400).send({
            response: 'Bad Request',
            errors: 'Accomodation Not Found',
          });
        }

        return status
          .destroy()
          .then(() => res.status(204).send({
            response: 'No Content',
            errors: null,
          }))
          .catch((error) => {
            res.status(400).send({
              response: 'Bad Request',
              errors: error
            });
          });
      })
      .catch((error) => {
        res.status(400).send({
          response: 'Bad Request',
          errors: error
        });
      });
  },


}