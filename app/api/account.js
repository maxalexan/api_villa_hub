require('dotenv').config()
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const Account = require('../models').account;
const secret = process.env.SECRET

module.exports = {
    // check account
    checkAccount(req, res, next) {
        Account.findOne({
            where: {
                email: req.body.email
            }
        }).then(account => {
            if (account) {
                res.status(400).send({
                    auth: false,
                    errors: "Email sudah terdaftar"
                });
                return;
            }
            next();

        });
    },
    // signup
    signup(req, res) {
        return Account
            .create({
                full_name: req.body.full_name,
                email: req.body.email,
                password: bcrypt.hashSync(req.body.password, 8)
            }).then(user => {
                res.status(200).send({
                    auth: true,
                    email: req.body.email,
                    message: "User registered successfully!",
                    errors: null,
                });


            }).catch(err => {
                res.status(500).send({
                    auth: false,
                    email: req.body.email,
                    message: "Error",
                    errors: err
                });
            })
    },
    // signin
	signin(req, res, next) {
		return Account
			.findOne({
				where: {
					email: req.body.email
				}
			}).then(account => {
				if (!account) {
					return res.status(404).send({
						auth: false,
                        email: req.body.email,
						accessToken: null,
						message: "Error",
						errors: "Email tidak ditemukan."
					});
				}

				var passwordIsValid = bcrypt.compareSync(req.body.password, account.password);
				if (!passwordIsValid) {
					return res.status(401).send({
						auth: false,
                        email: req.body.email,
						accessToken: null,
						message: "Error",
						errors: "Invalid Password!"
					});
				}

				var token = 'Bearer ' + jwt.sign({
                    email: req.body.email,
				}, process.env.SECRET, {
					expiresIn: 86400 //24h expired
				});

				res.status(200).send({
					auth: true,
					email: req.body.email,
					accessToken: token,
					message: "Success generate token",
					errors: null
				});
			}).catch(err => {
				res.status(500).send({
					auth: false,
					email: req.body.email,
					accessToken: null,
					message: "Error",
					errors: err
				});
			});next()
	},
	// verify token
	verifyToken(req, res, next) {
		let tokenHeader = req.headers['x-access-token'];

		if (tokenHeader.split(' ')[0] !== 'Bearer') {
			return res.status(500).send({
				auth: false,
				message: "Error",
				errors: "Incorrect token format"
			});
		}

		let token = tokenHeader.split(' ')[1];

		if (!token) {
			return res.status(403).send({
				auth: false,
				message: "Error",
				errors: "No token provided"
			});
		}

		jwt.verify(token, secret, (err, decoded) => {
			if (err) {
				return res.status(500).send({
					auth: false,
					message: "Error",
					errors: err
				});
			}
			req.userId = decoded.id;
			next();
		});
	},

	list(req, res) {
		return Account
		  .findAll({
			where: {
					email: req.body.email
				}
		  })
		  .then(docs => {
			const statuses = {
			  response: 'OK',
			  count: docs.length,
			  statuses: docs.map(doc => {
				return doc
			  }),
			  errors: null
			}
			res.status(200).send(statuses);
		  })
		  .catch((error) => {
			res.status(400).send({
			  response: 'Bad Request',
			  errors: error
			});
		  });
	  },
	
	// roles masih error
	// // roles account
	// isAdmin(req, res, next) {
	// 	Account.findByPk(req.type_account)
	// 		.then(account => {
	// 			account.getRoles().then(account => {
	// 				for (let i = 0; i < account.length; i++) {
	// 					console.log(account[i].type_account);
	// 					if (account[i].type_account() === 1) {
	// 						next();
	// 						return;
	// 					}
	// 				}
	// 				res.status(403).send({
	// 					auth: false,
	// 					message: "Error",
	// 					message: 'Require Admin Role',
	// 				});
	// 				return;
	// 			})
	// 		})
	// },
    


}