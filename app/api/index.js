const account = require('./account');
const accomodations = require('./accomodations');
const room = require('./room');
const amenities = require('./amenities');
module.exports = {
    account,
    accomodations,
    room,
    amenities,
};