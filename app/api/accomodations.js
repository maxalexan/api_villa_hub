const Accomodation = require('../models').accomodations;

module.exports = {

  getById(req, res) {
    return Accomodation
      .findByPk(req.params.id, {
        include: [],
      })
      .then((accomodations) => {
        if (!accomodations) {
          return res.status(404).send({
            response: 'Not Found',
            errors: 'Accomodation Not Found',
          });
        }
        const status = {
          response: 'OK',
          status: accomodations,
          errors: null
        }
        return res.status(200).send(status);
      })
      .catch((error) => {
        res.status(400).send({
          response: 'Bad Request',
          errors: error
        });
      });
  },

  list(req, res) {
    return Accomodation
      .findAll({
        limit: 10,
        include: [],
        order: [
          ['createdAt', 'DESC']
        ],
      })
      .then(docs => {
        const statuses = {
          response: 'OK',
          count: docs.length,
          statuses: docs.map(doc => {
            return doc
          }),
          errors: null
        }
        res.status(200).send(statuses);
      })
      .catch((error) => {
        res.status(400).send({
          response: 'Bad Request',
          errors: error
        });
      });
  },

  
  add(req, res) {
    return Accomodation
      .create({
        acc_name: req.body.acc_name,
      })
      .then((doc) => {
        const status = {
          response: 'Accomodations has Created',
          status: doc,
          errors: null
        }
        return res.status(201).send(status);
      })
      .catch((error) => {
        res.status(400).send({
          response: 'Bad Request',
          errors: error
        });
      });
  },

  update(req, res) {
    return Accomodation
      .findByPk(req.params.id, {})
      .then(status => {
        if (!status) {
          return res.status(404).send({
            response: 'Bad Request',
            errors: 'Accomoddations Not Found',
          });
        }

        return status
          .update({
            acc_name: req.body.acc_name || accomodation.acc_name
          })
          .then((doc) => {
            const status = {
              response: 'OK',
              status: doc,
              errors: null
            }
            return res.status(200).send(status);
          })
          .catch((error) => {
            res.status(400).send({
              response: 'Bad Request',
              errors: error
            });
          });
      })
      .catch((error) => {
        res.status(400).send({
          response: 'Bad Request',
          errors: error
        });
      });
  },

  delete(req, res) {
    return Accomodation
      .findByPk(req.params.id)
      .then(status => {
        if (!status) {
          return res.status(400).send({
            response: 'Bad Request',
            errors: 'Accomodation Not Found',
          });
        }

        return status
          .destroy()
          .then(() => res.status(204).send({
            response: 'No Content',
            errors: null,
          }))
          .catch((error) => {
            res.status(400).send({
              response: 'Bad Request',
              errors: error
            });
          });
      })
      .catch((error) => {
        res.status(400).send({
          response: 'Bad Request',
          errors: error
        });
      });
  },


}