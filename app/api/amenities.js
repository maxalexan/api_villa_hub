const Amenities = require('../models').amenities;

module.exports = {

  getById(req, res) {
    return Amenities
      .findByPk(req.params.id, {
        include: [],
      })
      .then((amenities) => {
        if (!amenities) {
          return res.status(404).send({
            response: 'Not Found',
            errors: 'Amenities Not Found',
          });
        }
        const status = {
          response: 'OK',
          status: amenities,
          errors: null
        }
        return res.status(200).send(status);
      })
      .catch((error) => {
        res.status(400).send({
          response: 'Bad Request',
          errors: error
        });
      });
  },

  list(req, res) {
    return Amenities
      .findAll({
        limit: 10,
        include: [],
        order: [
          ['createdAt', 'DESC']
        ],
      })
      .then(docs => {
        const statuses = {
          response: 'OK',
          count: docs.length,
          statuses: docs.map(doc => {
            return doc
          }),
          errors: null
        }
        res.status(200).send(statuses);
      })
      .catch((error) => {
        res.status(400).send({
          response: 'Bad Request',
          errors: error
        });
      });
  },

  
  add(req, res) {
    return Amenities
      .create({
        name: req.body.name,
        amtype: req.body.amtype,
      })
      .then((doc) => {
        const status = {
          response: 'Amenities has Created',
          status: doc,
          errors: null
        }
        return res.status(201).send(status);
      })
      .catch((error) => {
        res.status(400).send({
          response: 'Bad Request',
          errors: error
        });
      });
  },

  update(req, res) {
    return Amenities
      .findByPk(req.params.id, {})
      .then(status => {
        if (!status) {
          return res.status(404).send({
            response: 'Bad Request',
            errors: 'Amenities Not Found',
          });
        }

        return status
          .update({
            name: req.body.name || amenities.name,
            amtype: req.body.amtype || amenities.amtype
          })
          .then((doc) => {
            const status = {
              response: 'OK',
              status: doc,
              errors: null
            }
            return res.status(200).send(status);
          })
          .catch((error) => {
            res.status(400).send({
              response: 'Bad Request',
              errors: error
            });
          });
      })
      .catch((error) => {
        res.status(400).send({
          response: 'Bad Request',
          errors: error
        });
      });
  },

  delete(req, res) {
    return Amenities
      .findByPk(req.params.id)
      .then(status => {
        if (!status) {
          return res.status(400).send({
            response: 'Bad Request',
            errors: 'Accomodation Not Found',
          });
        }

        return status
          .destroy()
          .then(() => res.status(204).send({
            response: 'No Content',
            errors: null,
          }))
          .catch((error) => {
            res.status(400).send({
              response: 'Bad Request',
              errors: error
            });
          });
      })
      .catch((error) => {
        res.status(400).send({
          response: 'Bad Request',
          errors: error
        });
      });
  },


}