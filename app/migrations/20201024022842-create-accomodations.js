'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('accomodations', {
      id: {
        allowNull: false,
        primaryKey: true,
        type: Sequelize.UUID
      },
      acc_name: {
        type: Sequelize.STRING
      },
      acc_slug: {
        type: Sequelize.STRING
      },
      acc_desc: {
        type: Sequelize.STRING
      },
      acc_stars: {
        type: Sequelize.INTEGER
      },
      acc_type: {
        type: Sequelize.INTEGER
      },
      acc_meta_title: {
        type: Sequelize.STRING
      },
      acc_meta_keyword: {
        type: Sequelize.STRING
      },
      acc_status: {
        type: Sequelize.INTEGER
      },
      acc_email: {
        type: Sequelize.STRING
      },
      acc_phone: {
        type: Sequelize.STRING
      },
      acc_policy: {
        type: Sequelize.STRING
      },
      location_id: {
        type: Sequelize.INTEGER
      },
      account_id: {
        type: Sequelize.INTEGER
      },
      amenities_id: {
        type: Sequelize.INTEGER
      },
      acc_thumb: {
        type: Sequelize.STRING
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('accomodations');
  }
};