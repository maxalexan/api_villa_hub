'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('rooms', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      id_hotel: {
        type: Sequelize.INTEGER
      },
      room_hotel: {
        type: Sequelize.STRING
      },
      room_title: {
        type: Sequelize.STRING
      },
      room_basic_price: {
        type: Sequelize.DOUBLE
      },
      room_desc: {
        type: Sequelize.STRING
      },
      room_cap: {
        type: Sequelize.INTEGER
      },
      room_amenities: {
        type: Sequelize.STRING
      },
      room_add: {
        type: Sequelize.INTEGER
      },
      room_quantity: {
        type: Sequelize.INTEGER
      },
      room_status: {
        type: Sequelize.INTEGER
      },
      cover_img: {
        type: Sequelize.STRING
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('rooms');
  }
};