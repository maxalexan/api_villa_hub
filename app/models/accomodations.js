'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class accomodations extends Model { 
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  accomodations.init({
    id: {
      type: DataTypes.UUID,
      defaultValue: DataTypes.UUIDV4,
      primaryKey: true,
      allowNull: false 
    },
    acc_name: DataTypes.STRING,
    acc_slug: DataTypes.STRING,
    acc_desc: DataTypes.STRING,
    acc_stars: DataTypes.INTEGER,
    acc_type: DataTypes.INTEGER,
    acc_meta_title: DataTypes.STRING,
    acc_meta_keyword: DataTypes.STRING,
    acc_status: DataTypes.INTEGER,
    acc_email: DataTypes.STRING,
    acc_phone: DataTypes.STRING,
    acc_policy: DataTypes.STRING,
    location_id: DataTypes.INTEGER,
    account_id: DataTypes.INTEGER,
    amenities_id: DataTypes.INTEGER,
    acc_thumb: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'accomodations',
  });
  return accomodations;
};