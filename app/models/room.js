'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class room extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  room.init({
    id_hotel: DataTypes.INTEGER,
    room_hotel: DataTypes.STRING,
    room_title: DataTypes.STRING,
    room_basic_price: DataTypes.DOUBLE,
    room_desc: DataTypes.STRING,
    room_cap: DataTypes.INTEGER,
    room_amenities: DataTypes.STRING,
    room_add: DataTypes.INTEGER,
    room_quantity: DataTypes.INTEGER,
    room_status: DataTypes.INTEGER,
    cover_img: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'room',
  });
  return room;
};