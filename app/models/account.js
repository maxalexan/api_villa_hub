'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class account extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  account.init({
    full_name: DataTypes.STRING,
    email: DataTypes.STRING, 
    password: DataTypes.STRING,
    address: DataTypes.STRING,
    phone: DataTypes.INTEGER,
    image: DataTypes.STRING,
    type_account: DataTypes.INTEGER,
    is_active: DataTypes.INTEGER,
    is_verified: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'account',
  });
  return account;
};