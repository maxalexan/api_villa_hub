const accountController = require("../api").account;
const accomodationsController = require("../api").accomodations;
const roomController = require("../api").room;
const amenitiesController = require("../api").amenities;

module.exports = function (app) {
  // User signup
  app.post(
    "/auth/signup",
    [accountController.checkAccount],
    accountController.signup
  );
  // User Signin
  app.post("/auth/signin", accountController.signin);

  app.get("/auth/login", accountController.list);  

  // Accomodations
  app.get("/accomodation", accomodationsController.list);

  //get id accomodation
  app.get(
    "/accomodation/:id",
    [
      accountController.verifyToken,
      // accountController.isAdmin (masih error)
    ],
    accomodationsController.getById
  );

  app.post(
    "/accomodation",
    [
      accountController.verifyToken,
      // accountController.isAdmin
    ],
    accomodationsController.add
  );

  app.put(
    "/accomodation/:id",
    [accountController.verifyToken],
    accomodationsController.update
  );

  app.delete(
    "/accomodation/:id",
    [accountController.verifyToken],
    accomodationsController.delete
  );

   //room
   app.get("/room", roomController.list);

   app.get(
    "/room/:id",
    [
      accountController.verifyToken,
      // accountController.isAdmin (masih error)
    ],
    roomController.getById
  );

  app.post(
    "/room",
    [
      accountController.verifyToken,
      // accountController.isAdmin
    ],
    roomController.add
  );

  app.put(
    "/room/:id",
    [accountController.verifyToken],
    roomController.update
  );

  app.delete(
    "/room/:id",
    [accountController.verifyToken],
    roomController.delete
  );

  //amenities
  app.get("/amenities", amenitiesController.list);

   app.get(
    "/amenities/:id",
    [
      accountController.verifyToken,
      // accountController.isAdmin (masih error)
    ],
    amenitiesController.getById
  );

  app.post(
    "/amenities",
    [
      accountController.verifyToken,
      // accountController.isAdmin
    ],
    amenitiesController.add
  );

  app.put(
    "/amenities/:id",
    [accountController.verifyToken],
    amenitiesController.update
  );

  app.delete(
    "/amenities/:id",
  [accountController.verifyToken],
  amenitiesController.delete
);
};
